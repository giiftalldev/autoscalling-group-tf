variable "name" {
  type = "string"
}

variable "ami_id" {
  type = "string"
}

variable instance_type {
  type    = "string"
  default = "t2.micro"
}

variable "instance_profile_name" {
  type    = "string"
  default = ""
}

variable "instance_security_groups" {
  type    = "list"
  default = []
}

variable "instance_target_group_arns" {
  type    = "list"
  default = []
}

variable "subnet_ids" {
  type = "list"
}

variable "key_name" {
  type    = "string"
  default = ""
}

variable "service_linked_role_arn" {
  type = "string"
}
