resource "aws_launch_configuration" "launch-config" {
  image_id             = "${var.ami_id}"
  instance_type        = "${var.instance_type}"
  iam_instance_profile = "${var.instance_profile_name}"
  name_prefix          = "${var.name}-"

  lifecycle {
    create_before_destroy = true
  }

  security_groups   = ["${var.instance_security_groups}"]
  enable_monitoring = true
  key_name          = "${var.key_name}"
}

resource "aws_autoscaling_group" "asg" {
  name_prefix          = "${var.name}-"
  launch_configuration = "${aws_launch_configuration.launch-config.name}"
  vpc_zone_identifier  = ["${var.subnet_ids}"]

  target_group_arns = ["${var.instance_target_group_arns}"]

  max_size = 8
  min_size = 1

  service_linked_role_arn = "${var.service_linked_role_arn}"

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]

  health_check_grace_period = 180

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "scale-up-vpc" {
  name                   = "scale_up"
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = 1
  autoscaling_group_name = "${aws_autoscaling_group.asg.name}"
  cooldown               = 60
}

resource "aws_autoscaling_policy" "scale-down-vpc" {
  name                   = "scale_down"
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = -1
  autoscaling_group_name = "${aws_autoscaling_group.asg.name}"
  cooldown               = 60
}
